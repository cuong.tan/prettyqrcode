import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import qr from 'qrcode';
import PropTypes from 'prop-types';

import FinderPattern from './FinderPattern';
import {getMatrixSize} from './utils';

const {Position} = FinderPattern;

class PrettyQRCode_SVG extends Component {
  static propTypes = {
    size: PropTypes.number,
    value: PropTypes.string.isRequired,
    color: PropTypes.string,
    backgroundColor: PropTypes.string,
    errorCorrectionLevel: PropTypes.string,
    borderRadius: PropTypes.number,
    squareColor: PropTypes.string,
    logo: PropTypes.string,
    logoSize: PropTypes.number,
  };

  static defaultProps = {
    size: 100,
    color: '#000',
    backgroundColor: '#fff',
    errorCorrectionLevel: 'M',
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.value !== nextProps.value) {
      return {
        matrixSize: getMatrixSize(
          nextProps.value,
          nextProps.errorCorrectionLevel,
        ),
        value: nextProps.value,
      };
    }

    return null;
  }

  constructor(props) {
    super(props);

    this.state = {
      matrixSize: getMatrixSize(props.value, props.errorCorrectionLevel),
      value: props.value,
    };
  }

  get moduleSize() {
    return this.props.size / this.state.matrixSize;
  }

  render() {
    const {
      size,
      value,
      color,
      backgroundColor,
      errorCorrectionLevel,
      borderRadius,
      logo,
      logoSize,
      squareColor,
    } = this.props;

    return (
      <View
        style={[
          styles.prettyQRCode,
          {
            width: size,
            height: size,
          },
        ]}>
        <QRCode
          value={value}
          color={color}
          size={size}
          ecl={errorCorrectionLevel}
          logo={logo}
          logoSize={logoSize}
          backgroundColor={backgroundColor}
        />
        <FinderPattern
          color={color}
          backgroundColor={backgroundColor}
          moduleSize={this.moduleSize}
          position={Position.TOP_LEFT}
          borderRadius={borderRadius}
          squareColor={squareColor || color}
        />
        <FinderPattern
          color={color}
          backgroundColor={backgroundColor}
          moduleSize={this.moduleSize}
          borderRadius={borderRadius}
          position={Position.TOP_RIGHT}
          squareColor={squareColor || color}
        />
        <FinderPattern
          color={color}
          backgroundColor={backgroundColor}
          moduleSize={this.moduleSize}
          borderRadius={borderRadius}
          position={Position.BOTTOM_LEFT}
          squareColor={squareColor || color}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  prettyQRCode: {
  },
  row: {
    flexDirection: 'row',
  },
});

export default PrettyQRCode_SVG;
