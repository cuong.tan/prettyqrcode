import QRCodeEncoder from 'qrcode';

const getMatrixSize = (value, errorCorrectionLevel) => {
  const modules = Array.prototype.slice.call(
    QRCodeEncoder.create(value, {errorCorrectionLevel}).modules.data,
    0,
  );

  return Math.sqrt(modules.length);
};

const isFinderPatternModules = (x, y, n) => {
  const patternModules = [
    [0, 0, 6, 6],
    [n - 7, 0, n - 1, 6],
    [0, n - 7, 6, n - 1],
  ];

  return patternModules.some(patternModule => {
    const [xLow, yLow, xHigh, yHigh] = patternModule;

    return xLow <= x && x <= xHigh && yLow <= y && y <= yHigh;
  });
};

const generateMatrix = (value, errorCorrectionLevel) => {
  const modules = Array.prototype.slice.call(
    QRCode.create(value, {errorCorrectionLevel}).modules.data,
    0,
  );
  const matrixSize = Math.sqrt(modules.length);

  return modules.reduce(
    (rows, module, index) =>
      (index % matrixSize === 0
        ? rows.push([module])
        : rows[rows.length - 1].push(module)) && rows,
    [],
  );
};

export {
  getMatrixSize,
  isFinderPatternModules,
  generateMatrix,
};
