import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import QRCode from 'qrcode';
import PropTypes from 'prop-types';

import FinderPattern from './FinderPattern';
import {generateMatrix} from './utils';

class PrettyQRCode extends Component {
  static propTypes = {
    size: PropTypes.number,
    value: PropTypes.string.isRequired,
    color: PropTypes.string,
    backgroundColor: PropTypes.string,
    borderRadius: PropTypes.number,
  };

  static defaultProps = {
    size: 100,
    color: '#000',
    backgroundColor: '#fff',
    borderRadius: 0,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.value !== nextProps.value) {
      return {
        qrCodeMatrix: generateMatrix(nextProps.value),
        value: nextProps.value,
      };
    }

    return null;
  }

  constructor(props) {
    super(props);

    const qrCodeMatrix = generateMatrix(props.value);

    this.state = {
      qrCodeMatrix,
      value: props.value,
    };
  }

  get moduleSize() {
    const qrMatrixLength = this.state.qrCodeMatrix.length;

    return qrMatrixLength === 0
      ? 0
      : Math.floor(this.props.size / qrMatrixLength);
  }

  renderModule = (isModule, x, y) => {
    const {color, backgroundColor} = this.props;

    return (
      <View
        key={y}
        style={{
          width: this.moduleSize,
          height: this.moduleSize,
          backgroundColor: isModule ? color : backgroundColor,
        }}
      />
    );
  };

  renderRow = (row = [], rowIndex) => {
    if (row.length === 0) {
      return null;
    }

    return (
      <View key={rowIndex} style={styles.row}>
        {row.map((module, colIndex) =>
          this.renderModule(module, rowIndex, colIndex),
        )}
      </View>
    );
  };

  render() {
    const {color, backgroundColor, borderRadius} = this.props;
    const {qrCodeMatrix = []} = this.state;

    return (
      <View style={styles.prettyQRCode}>
        {qrCodeMatrix.map(this.renderRow)}
        <FinderPattern
          top={0}
          left={0}
          color={color}
          backgroundColor={backgroundColor}
          moduleSize={this.moduleSize}
          borderRadius={borderRadius}
        />
        <FinderPattern
          top={0}
          right={0}
          color={color}
          backgroundColor={backgroundColor}
          moduleSize={this.moduleSize}
          borderRadius={borderRadius}
        />
        <FinderPattern
          bottom={0}
          left={0}
          color={color}
          backgroundColor={backgroundColor}
          moduleSize={this.moduleSize}
          borderRadius={borderRadius}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  prettyQRCode: {
    position: 'absolute',
    flexDirection: 'column',
  },
  row: {
    flexDirection: 'row',
  },
});

export default PrettyQRCode;
