import {View} from 'react-native';
import React from 'react';

const Position = {
  TOP_RIGHT: 'TOP_RIGHT',
  TOP_LEFT: 'TOP_LEFT',
  BOTTOM_LEFT: 'BOTTOM_LEFT',
};

const PositionCoordinate = {
  [Position.TOP_RIGHT]: {
    top: 0,
    right: 0,
  },
  [Position.TOP_LEFT]: {
    top: 0,
    left: 0,
  },
  [Position.BOTTOM_LEFT]: {
    bottom: 0,
    left: 0,
  },
};

const FinderPattern = ({
  moduleSize,
  color,
  backgroundColor,
  position,
  borderRadius,
  squareColor,
} = {}) => {
  return (
    <View
      style={{
        backgroundColor,
        position: 'absolute',
        height: moduleSize * 8,
        width: moduleSize * 8,
      }}
      {...PositionCoordinate[position]}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: moduleSize * 7,
          width: moduleSize * 7,
          borderRadius,
          borderWidth: moduleSize,
          borderColor: color,
          backgroundColor: 'transparent',
          position: 'absolute',
        }}
        {...PositionCoordinate[position]}>
        <View
          style={{
            height: moduleSize * 3,
            width: moduleSize * 3,
            backgroundColor: squareColor || color,
          }}
        />
      </View>
    </View>
  );
};

FinderPattern.Position = Position;

export default FinderPattern;
